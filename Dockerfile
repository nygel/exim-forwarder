FROM alpine:3.10

# build layers
RUN apk add --no-cache exim
RUN mkdir -p /var/log/exim
RUN touch /var/log/exim/mainlog
RUN chown -R exim.exim /var/log/exim
RUN mkdir -p /usr/lib/exim
RUN chown exim.exim /usr/lib/exim
RUN cp /etc/exim/exim.conf /etc/exim/exim.conf_backup

# config layers
EXPOSE 25
COPY exim.conf /etc/exim/

CMD ["/usr/sbin/exim", "-bdf", "-q30m"]

# Exim Forwarder

Forwards all email for a specified domain onto a specified email address.

Can be pulled from Docker hub : https://hub.docker.com/r/nygel/exim-forwarder/

Instructions for use can be found on Docker hub. 
